Feature: pesan
  Narrative:
  In order to resolve my hungry
  As a Customer
  I want to order food

  Scenario: Pesan makanan
    Given Saya memesan makanan "Pizza" dan Jumlah makanan nya "2"
    When Saya ingin memesan makanan
    Then Saya melihat menu

  Scenario: Pesan Minuman
    Given Saya ingin memesan minuman "ThaiTea" dan Jumlah minuman nya "3"
    When Saya ingin memesan minuman
    Then Saya melihat menu

  Scenario: Pesan Makanan dan Minuman
    Given Saya ingin memesan makanan "Pizza" dan minuman "ThaiTea" dan Jumlah makanan nya "2" dan minuman nya "3"
    When Saya ingin memesan makanan dan minuman
    Then Saya melihat menu