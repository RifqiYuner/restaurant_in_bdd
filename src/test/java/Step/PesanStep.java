package Step;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PesanStep {
    @Given("^Saya memesan makanan \"([^\"]*)\"$")
    public void sayaMemesanMakanan(String Pizza) throws Throwable {
        System.out.println("Test"+Pizza);
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^Jumlah makanan nya \"([^\"]*)\"$")
    public void jumlahMakananNya(Integer a) throws Throwable {
        System.out.println("Test" + a);
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^Saya ingin memesan makanan$")
    public void sayaInginMemesanMakanan() {
    }

    @Then("^Saya melihat menu$")
    public void sayaMelihatMenu() {
    }

    @Given("^Saya ingin memesan minuman \"([^\"]*)\"$")
    public void sayaInginMemesanMinuman(String ThaiTea) throws Throwable {
        System.out.println("Test" + ThaiTea);
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^Jumlah minuman nya \"([^\"]*)\"$")
    public void jumlahMinumanNya(Integer a) throws Throwable {
        System.out.println("Test" + a);
        //Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^Saya ingin memesan minuman$")
    public void sayaInginMemesanMinuman() {
    }

    @Given("^Saya ingin memesan makanan \"([^\"]*)\" dan minuman \"([^\"]*)\"$")
    public void sayaInginMemesanMakananDanMinuman(String Pizza, String ThaiTea) throws Throwable {
        System.out.println("Test" +Pizza + ThaiTea);
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @And("^Jumlah makanan nya \"([^\"]*)\" dan minuman nya \"([^\"]*)\"$")
    public void jumlahMakananNyaDanMinumanNya(Integer a, Integer b) throws Throwable {
        System.out.println("Test" +a +b);
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^Saya ingin memesan makanan dan minuman$")
    public void sayaInginMemesanMakananDanMinuman() {
    }
}
